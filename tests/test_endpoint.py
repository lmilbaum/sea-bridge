#!/usr/bin/env python3

"""Test endpoint.py"""

import pytest

import sea_bridge.endpoint


@pytest.mark.parametrize(
    "url_endpoint",
    [
        ("/"),
        ("/livez"),
        ("/readyz"),
    ],
)
def test_get_index_route(url_endpoint, fake_config, fake_scheduler, fake_sync_lock):
    """Test endpoint '/'

    Checks that the index route returns the expected response when accessed
    with HTTP GET. The expected response is a RESTful json dict showing the
    sea-bridge configuration (with secrets stripped).
    """
    expected_response = {
        "artifactory_repo": fake_config["artifactory"]["artifactory_repo"],
        "artifactory_url": fake_config["artifactory"]["artifactory_url"],
        "artifactory_upload_prefix": fake_config["artifactory"][
            "artifactory_upload_prefix"
        ],
        "s3_bucket": fake_config["aws_s3"]["s3_bucket"],
        "s3_prefixes": fake_config["aws_s3"]["s3_prefixes"],
        "status": "ready",
    }

    flask_app = sea_bridge.endpoint.create_app(
        fake_config, fake_scheduler, fake_sync_lock
    )
    with flask_app.test_client() as test_client:
        response = test_client.get(url_endpoint)
        assert response.status_code == 200
        assert response.json
        assert response.json == expected_response


@pytest.mark.parametrize(
    "syncing, state",
    [
        (True, "syncing"),
        (False, "waiting"),
    ],
)
def test_get_sync_route(syncing, state, fake_config, fake_scheduler, fake_sync_lock):
    """Test endpoint '/sync'

    Checks that the /sync route returns the expected response when accessed
    with HTTP GET. The expected response is a RESTful json dict showing the
    sea-bridge syncing status. The sync_lock is parametrized so the test
    checks both states (syncing and waiting).
    """
    fake_sync_lock.am_locked = syncing
    expected_response = {
        "current_state": state,
        "pending_sync_jobs": [],
        "possible_states": ["waiting", "syncing"],
    }

    flask_app = sea_bridge.endpoint.create_app(
        fake_config, fake_scheduler, fake_sync_lock
    )
    with flask_app.test_client() as test_client:
        response = test_client.get("/sync")
        assert response.status_code == 200
        assert response.json
        assert response.json == expected_response


@pytest.mark.parametrize(
    "api_key_test, syncing, state",
    [
        (None, True, "syncing"),
        (None, False, "waiting"),
        (True, True, "syncing"),
        (True, False, "waiting"),
        (False, True, "syncing"),
        (False, False, "waiting"),
    ],
)
def test_post_sync_route(
    api_key_test,
    syncing,
    state,
    fake_api_keys,
    fake_config,
    fake_scheduler,
    fake_sync_lock,
):
    """Test endpoint '/sync' with POST

    Checks that POSTing to the /sync route returns the expected response.

    The tests check that on a successful trigger, the expected response is a RESTful
    json dict showing the sea-bridge syncing status.

    The tests also check the possible responses when the endpoint is protected with
    and api key. When the api_key_test is set to None, the tests assume no api_key is
    configured to protect the /sync endpoint. When api_key_test is True, the tests check
    the behaviour of the /sync endpoint when fed the valid api_key. When set to False,
    the test checks that the 403 error state is thrown and the endpoint is protected.

    The sync_lock is parametrized so the tests checks both states (syncing and
    waiting).

    The test does not check if a job gets appropriately added because the fake
    scheduler is not that featureful to handle that.
    """
    fake_sync_lock.am_locked = syncing

    for i in range(0, len(fake_api_keys)):
        if api_key_test is None:
            request_data = {}
            expected_status_code = 403
            expected_response = {"error": "403 Forbidden: no api_key provided"}
        elif api_key_test is True:
            request_data = {"api_key": fake_api_keys[i]["api_key"]}
            expected_status_code = 200
            expected_response = {
                "current_state": state,
                "pending_sync_jobs": [],
                "possible_states": ["waiting", "syncing"],
            }
        elif api_key_test is False:
            request_data = {"api_key": "random!"}
            expected_status_code = 403
            expected_response = {"error": "403 Forbidden: invalid api_key"}

        flask_app = sea_bridge.endpoint.create_app(
            fake_config, fake_scheduler, fake_sync_lock
        )
        with flask_app.test_client() as test_client:
            response = test_client.post("/sync", data=request_data)
            assert response.status_code == expected_status_code
            assert response.json
            assert response.json == expected_response
