#!/usr/bin/env python3

import argparse
import hashlib
import random
import string

DEFAULT_API_KEY_LENGTH = 20
DEFAULT_SALT_LENGTH = 20


def argparser() -> argparse.ArgumentParser:
    """Argparser configuration"""
    parser = argparse.ArgumentParser(
        description="Utility to generate api keys for sea-bridge config files."
    )
    parser.add_argument(
        "--salt",
        type=str,
        required=False,
        help="Salt for hash operations (default: random 20 chars)",
    )
    parser.add_argument(
        "--api-key",
        type=str,
        required=False,
        help="The api-key to hash (default: random 20 chars)",
    )

    return parser


def main() -> None:
    parser = argparser()
    args = parser.parse_args()

    characters = string.ascii_letters + string.digits + "!$%&*<=>?@^_~"
    random_salt = "".join(random.choice(characters) for _ in range(DEFAULT_SALT_LENGTH))
    random_api_key = "".join(
        random.choice(characters) for _ in range(DEFAULT_API_KEY_LENGTH)
    )

    salt = args.salt or random_salt
    api_key = args.api_key or random_api_key

    digest = hashlib.sha512((salt + api_key).encode("utf-8")).hexdigest()

    print(f"|----------{'-'*len(api_key)}-|")
    print(f"| API Key: {api_key} |")
    print(f"|----------{'-'*len(api_key)}-|")

    print(f"- sha512_digest: {digest}")
    print(f"  salt: {salt}")


if __name__ == "__main__":
    main()
