#!/usr/bin/env python3

import atexit
import os
import pathlib
import threading
import traceback

from typing import Any, Generator

import artifactory
import boto3

from apscheduler.executors.pool import ThreadPoolExecutor, ProcessPoolExecutor
from apscheduler.schedulers.background import BackgroundScheduler, BaseScheduler
from mypy_boto3_s3.service_resource import Bucket, ObjectSummary


def get_scheduler() -> BaseScheduler:
    """Get the scheduler that handles sync jobs."""
    # Set default job params which ensure only one sync job runs at a time
    executors = {
        "default": ThreadPoolExecutor(1),
        "processpool": ProcessPoolExecutor(1),
    }
    job_defaults = {"coalesce": True, "max_instances": 1}

    # Create a background scheduler that runs in parallel to our flask server
    scheduler = BackgroundScheduler(
        executors=executors, job_defaults=job_defaults, timezone="UTC"
    )

    # Ensure scheduler shutsdown on exit
    atexit.register(lambda: scheduler.shutdown())

    # Get the scheduler into the state where it'll start running jobs
    # when it begins to receive them
    scheduler.start()

    return scheduler


def schedule_sync(
    scheduler: BaseScheduler, config: dict[str, Any], sync_lock: threading.Lock
) -> None:
    """Add a sync job that runs on a regular interval."""
    scheduler.add_job(
        func=_sync_s3_to_artifactory,
        args=[config, sync_lock],
        trigger="interval",
        minutes=config["interval"],
    )


def trigger_sync(
    scheduler: BaseScheduler, config: dict[str, Any], sync_lock: threading.Lock
) -> None:
    """Add a once-off sync job that runs immediately."""
    print("immediate job")
    scheduler.add_job(
        func=_sync_s3_to_artifactory, args=[config, sync_lock], trigger="date"
    )


def _sync_s3_to_artifactory(config: dict[str, Any], sync_lock: threading.Lock) -> None:
    """Synchronises the S3 bucket to Artifactory as per the config."""
    # TODO: implement actual sync workflow
    sync_lock.acquire()
    print("Synchronising S3 bucket to Artifactory repo...")

    # Wrap the sync operations in try/except to avoid crashing the service if there are issues
    try:
        s3_bucket, artifactory_path = _connect_to_sync_resources(config)
        s3_objects = _get_s3_objects_to_sync(s3_bucket, config["aws_s3"]["s3_prefixes"])
        _sync_s3_objects_to_artifactory_repo(s3_bucket, artifactory_path, s3_objects)
    except Exception as sync_e:
        print(traceback.format_exc())
        print(f"Error: could not sync bucket to repo: {sync_e}")

    print("Synchronisation finished.")
    sync_lock.release()


def _connect_to_sync_resources(
    config: dict[str, Any]
) -> tuple[Bucket, artifactory.ArtifactoryPath]:
    """Create s3_bucket and artifactory_path resources"""
    # Establish s3 resources
    session = boto3.Session(
        aws_access_key_id=config["aws_s3"]["s3_credentials"]["access_key_id"],
        aws_secret_access_key=config["aws_s3"]["s3_credentials"]["secret_access_key"],
    )
    s3 = session.resource("s3")
    s3_bucket = s3.Bucket(config["aws_s3"]["s3_bucket"])

    # Establish Artifactory resources
    # Note: cannot pass full path directly here as it breaks the port param if there is one
    # eg. http://artifactory.local:8080/test -> http://artifactory.local/:8080/test
    # Workaround this by passing only the top level server uri then build the path on top.
    artifactory_server = artifactory.ArtifactoryPath(
        config["artifactory"]["artifactory_url"],
        token=config["artifactory"]["artifactory_credentials"]["access_token"],
    )

    artifactory_path = (
        artifactory_server
        / config["artifactory"]["artifactory_repo"]
        / config["artifactory"]["artifactory_upload_prefix"]
    )

    # Test artifactory_path access
    (artifactory_path / ".sea-bridge").touch()

    return s3_bucket, artifactory_path


def _get_s3_objects_to_sync(
    s3_bucket: Bucket, s3_prefixes: list[str]
) -> list[ObjectSummary]:
    """Collect list of s3 objects in s3_bucket to sync"""
    s3_objects = []

    # Determine ordered list of objects in bucket to sync
    # Ordered by prefix listed in config file
    for s3_prefix in s3_prefixes:
        s3_prefix = s3_prefix.strip("/")
        for s3_object in s3_bucket.objects.filter(Prefix=f"{s3_prefix}"):
            s3_objects.append(s3_object.key)

    return s3_objects


def _sync_s3_objects_to_artifactory_repo(
    s3_bucket: Bucket,
    artifactory_path: artifactory.ArtifactoryPath,
    s3_objects: list[str],
) -> None:
    """Synchronise s3_objects in s3_bucket to artifactory_path"""
    tmp_file_dir = pathlib.Path("/tmp")
    for s3_object in s3_objects:
        print(f"Syncing: {s3_object}")

        # No need to upload directories explicitly
        if s3_object.endswith("/"):
            continue

        # Download, calculate sha256, then upload (or skip if already in sync)
        s3_object_name = os.path.basename(s3_object)
        tmp_file = tmp_file_dir / s3_object_name
        s3_bucket.download_file(s3_object, tmp_file)
        _artifactory_upload(tmp_file, artifactory_path / s3_object)
        tmp_file.unlink()

    # Clean up stale files in artifactory_path
    _clean_artifactory_path(artifactory_path, s3_objects)


def _clean_artifactory_path(
    artifactory_path: artifactory.ArtifactoryPath, s3_objects: list[str]
) -> None:
    """Remove Artifactory files in artifactory_path that don't exist in s3_objects"""
    # collect s3 prefixes of s3 objects to use for validating artifactory_path files
    s3_prefixes = _s3_prefixes(s3_objects)

    # pre-walk the artifactory_path to avoid issues with deleting while walking
    paths = list(_walk_artifactory_path(artifactory_path))
    for path in paths:
        # ArtifactoryPath.relative_to is broken so use os.path.relpath
        # to workaround it.
        relative_path = os.path.relpath(str(path), str(artifactory_path))
        if relative_path == ".sea-bridge":
            continue
        if relative_path in s3_prefixes or relative_path in s3_objects:
            continue

        print(f"Deleting: {path}")
        try:
            path.unlink()
        except FileNotFoundError:
            # gracefully handle race condition deletes
            pass


def _walk_artifactory_path(
    artifactory_path: artifactory.ArtifactoryPath,
) -> Generator[artifactory.ArtifactoryPath, None, None]:
    """Helper function to walk artifactory_path with full paths returned"""
    # to workaround bugs in dohq-artifactory pathlib implementation, this uses
    # artifactory.walk instead of glob
    for root, dirs, files in artifactory.walk(artifactory_path):
        # do files first to make deletions a bit better sorted
        for file in files:
            path = root / file
            yield path
        for directory in dirs:
            path = root / directory
            yield path


def _s3_prefixes(s3_objects: list[str]) -> set[str]:
    """Collect s3 prefixes of s3 objects"""
    s3_dirs = set()
    for s3_object in s3_objects:
        prefix = os.path.dirname(s3_object)
        while prefix:
            s3_dirs.add(prefix)
            prefix = os.path.dirname(prefix)
    return s3_dirs


def _artifactory_upload(
    local_file: pathlib.Path, upload_path: artifactory.ArtifactoryPath
) -> None:
    """Wrapper for artifactory upload to try checksum-upload first"""
    # Calculate sha256
    local_file_sha256 = artifactory.sha256sum(local_file)

    # Check if file exists and already has the matching checksum
    if upload_path.exists() and upload_path.stat().sha256 == local_file_sha256:
        return

    # Try to upload by checksum to Artifactory
    try:
        upload_path.deploy_by_checksum(sha256=local_file_sha256)
        print(f"Uploaded-by-checksum: {local_file_sha256}")
        return
    except:
        pass

    # If upload-by-checksum fails, do a real upload
    upload_path.deploy_file(local_file)
    print(f"Uploaded: {local_file}")
